/*
 * @Author: Tran Van Nhut (nhutdev) 
 * @Date: 2017-11-30 17:24:04 
 * @Last Modified by: Tran Van Nhut (nhutdev)
 * @Last Modified time: 2018-02-21 12:58:46
 */
const config = require('config');
const rq = require('request-promise');
const Hoek = require('hoek');

class OAuth {
  static bearer(merge, handle, handlePermission, req, res, next) {
    const oAuthCfg = config.oAuth.bearer;
    Hoek.assert(oAuthCfg, 'Config OAuth Bearer cannot empty');

    return rq({
      uri: oAuthCfg.uri.validateToken,
      baseUrl: config.oAuth.baseUrl,
      method: 'PUT',
      headers: {
        authorization: OAuth.parseTokenBasic
      },
      body: {
        accessToken: OAuth.accessToken(req)
      },
      json: true
    })
      .then(async data => {
        const key = config.keyUser || 'user';

        if (handle) {
          const user = await handle(req, res, data.data);
          req.commonData[key] = user;
          if (handlePermission) {
            try {
              await handlePermission(req, res);
            } catch (error) {
              return req.replyError(req, res, error, {
                statusCode: '403'
              });
            }
          }
          return next();
        }
        req.commonData[key] = data.data;
        return next();
      })
      .catch(err => {
        // if (merge && err.error && Array.isArray(err.error.errors)) {
        //   const error = err.error.errors[0];
        //   if (error.code === '511') {
        //     return next();
        //   }
        // }
        return OAuth.exceptionForConnectServer(req, res, err);
      });
  }

  static get parseTokenBasic() {
    const cfgOAuthBasic = config.oAuth.basic.jwt;
    Hoek.assert(
      cfgOAuthBasic,
      'OAuth basic token config not null with key basic.jwt'
    );
    const basicToken = new Buffer(
      `${cfgOAuthBasic.clientSecret}.${cfgOAuthBasic.clientId}`
    ).toString('base64');
    return `Basic ${basicToken}`;
  }

  static merge(handle, handlePermission, req, res, next) {
    const authorization = req.headers.authorization;
    if (/^Basic /.test(authorization)) {
      return OAuth.basic(req, res, next);
    }
    return OAuth.bearer(true, handle, handlePermission, req, res, next);
  }

  static exceptionForConnectServer(req, res, error) {
    if (error.cause && error.cause.code === 'ECONNREFUSED') {
      return req.replyError(req, res, {
        code: '513',
        source: error.message
      });
    }

    let errorCurrent,
      sourceMess = error.response
        ? `Error from url: ${error.response.request.uri.href}`
        : null;
    if (error.code && !error.error) {
      errorCurrent = error;
    } else if (Array.isArray(error.error.errors)) {
      errorCurrent = error.error.errors;
    } else {
      errorCurrent = error.error.errors[0];
    }

    return req.replyError(req, res, errorCurrent);
  }

  static accessToken(req) {
    return req.headers.authorization.split(' ')[1];
  }

  static basic(req, res, next) {
    const oAuthCfg = config.oAuth.basic;
    Hoek.assert(oAuthCfg, 'Config OAuth Basic cannot empty');

    return rq({
      uri: oAuthCfg.uri.validateBasic,
      baseUrl: config.oAuth.baseUrl,
      method: 'PUT',
      headers: {
        authorization: OAuth.parseTokenBasic
      },
      body: {
        accessToken: OAuth.accessToken(req)
      },
      json: true
    })
      .then(() => {
        return next();
      })
      .catch(err => {
        return OAuth.exceptionForConnectServer(req, res, err);
      });
  }
}

module.exports = OAuth;
