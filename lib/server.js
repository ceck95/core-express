const express = require('express');
const path = require('path');
// const favicon = require('serve-favicon');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const device = require('express-device');

const app = express();
const dataStore = new (require('../src/data-store'))();
const getStore = dataStore.getStore.bind(dataStore);
const BaseResponse = require('../src/base-response');
const log = require('./log');
const logHelper = require('../src/helper/log');

const BPromise = require('bluebird');
const fs = BPromise.promisifyAll(require('fs'));
const http = require('http');
const config = require('../src/config/index');
const Hoek = require('hoek');

//module swagger
const GeneratorSwagger = require('../src/swagger/generator');
const Render = require('../src/swagger/render');
const RenderSocket = require('../src/socket/render');

const cache = require('./cache');
const errorHelper = require('../src/error');

//module support worker
const cluster = require('cluster');
const os = require('os');

//Build
const BuildRequest = require('../src/build/request');

const Postman = require('../src/postman/index');
const Merge = require('../src/postman/merge');

// view engine setup
app.set('view options', {
  layout: true
});

app.set('views', path.join(process.cwd(), 'views'));
app.set('view engine', 'pug');

// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(cookieParser());
app.use(express.static(path.join(process.cwd(), 'public')));
app.use(
  express.static(
    path.join(process.cwd(), 'node_modules', 'core-express', 'public')
  )
);

app.use(
  device.capture({
    parseUserAgent: true
  })
);

class ServerExpress {
  constructor(configServer, opts) {
    this._config = configServer;

    if (config.jwt) {
      this._config.jsonWebToken = true;
    }

    if (config.oAuth) {
      this._config.oAuth = true;
    }

    this._opts = opts;
    if (config.cache && config.cache.clearCache) {
      cache
        .clearCache()
        .then(() => {
          log.info('Clear cache successfully');
        })
        .catch(err => {
          log.error(`Error clear cache ${err.toString()}`);
        });
    }
  }

  workerProcess(server) {
    const argument = process.argv[2];
    if (argument && argument === '--debug') {
      log.info(`Worker start with pid: ${process.pid}`);

      return server();
    }

    if (cluster.isMaster) {
      log.info(`Worker process with pid: ${process.pid}`);
      const cpus = os.cpus().length,
        cpusConfig = this._config.cluster ? this._config.cluster.cpus : null;
      if (cpusConfig)
        Hoek.assert(
          cpus >= cpusConfig,
          'Cpu length must be less small than max cpu length'
        );
      const length = cpusConfig || cpus;

      for (let i = 0; i < length; i++) {
        const worker = cluster.fork();
      }

      cluster.on('exit', (worker, code, signal) => {
        log.fatal(
          `[Cluster] worker exit with pid ${
            worker.process.pid
          } at ${new Date()}`
        );
        if (!worker.exitedAfterDisconnect) {
          const worker = cluster.fork();
        }
      });
    } else {
      log.info(`Worker start with pid: ${process.pid}`);

      return server();
    }
  }

  beforeRoute(req, res, next) {
    const routeList = Object.keys(this._genRouteSwagger),
      handleCurrent = req.route.stack[req.route.stack.length - 1];

    routeList.forEach(e => {
      this._genRouteSwagger[e].every(a => {
        if (a.url === req.route.path && a.method === handleCurrent.method) {
          req.route.controller = {
            name: a.controller.constructor.name,
            action: handleCurrent.name.replace('bound ', '')
          };
          return false;
        }
        return true;
      });
    });
    next();
  }

  setupPermission() {
    const self = this,
      routeList = Object.keys(self._genRouteSwagger);
    let controllers = [];
    const getObject = (controllerName, routes) => {
      let action = [];
      routes.forEach(e => {
        if (e.controller.constructor.name === controllerName) {
          const indexHandle = e.middleware.length - 1;
          const middlewareCurrent =
            typeof e.middleware === 'string'
              ? e.middleware
              : e.middleware[indexHandle];
          Hoek.assert(
            e.controller[middlewareCurrent],
            `Middleware ${middlewareCurrent} is not exist in controller ${
              e.controller.constructor.name
            }`
          );
          action.push({
            name: middlewareCurrent,
            description: e.summary
          });
        }
      });
      return {
        name: controllerName,
        actions: action
      };
    };
    routeList.forEach(b => {
      let controllerForRoute = {
        routeName: b,
        controllers: []
      };
      self._genRouteSwagger[b].forEach(e => {
        if (controllerForRoute.controllers.length === 0)
          controllerForRoute.controllers.push(
            getObject(e.controller.constructor.name, self._genRouteSwagger[b])
          );
        else
          controllerForRoute.controllers.every((a, y) => {
            if (e.controller.constructor.name === a.name) return false;
            if (controllerForRoute.controllers.length - 1 === y)
              controllerForRoute.controllers.push(
                getObject(
                  e.controller.constructor.name,
                  self._genRouteSwagger[b]
                )
              );
            return true;
          });
      });
      controllers.push(controllerForRoute);
    });
    const pathPermission = 'permission/setup.js',
      fullPath = `${process.cwd()}/${pathPermission}`;
    return fs.exists(fullPath, exist => {
      if (!exist) return null;
      this._startServer = false;
      const classSetup = require(fullPath);
      const setup = new classSetup();
      Hoek.assert(
        setup.run,
        '[Setup permission] Class setup not empty function with name run'
      );
      setup.run(getStore, controllers, log);
      return null;
    });
  }

  handleNotFound(req, res) {
    const configServer = this._config;
    let templateNotFound = configServer.template.notFound.default,
      handleRequestNotFound,
      checkRootMain = true;
    if (configServer.template.notFound.customize)
      configServer.template.notFound.customize.every(e => {
        const regRoute = new RegExp(`/${e.route}/`, 'g');
        if (regRoute.test(req.path)) {
          templateNotFound = e.templateNotFound;
          checkRootMain = false;
          if (this._notFound && this._notFound[e.route]) {
            handleRequestNotFound = this._notFound[e.route];
          }
          return false;
        }
        return true;
      });
    if (!handleRequestNotFound && checkRootMain) {
      if (this._notFound && this._notFound['main']) {
        handleRequestNotFound = this._notFound['main'];
      }
    }

    if (handleRequestNotFound) {
      return handleRequestNotFound(req, res).then(resp => {
        return req.reply(req, res, resp, templateNotFound, {
          status: 404
        });
      });
    }

    return req.reply(req, res, {}, templateNotFound, {
      status: 404
    });
  }

  securePath(req, res) {
    return new BPromise((resolve, reject) => {
      if (/'|"/g.test(req.path)) {
        if (/(^\/api\/)/g.test(req.path)) {
          return req.replyError(req, res, {
            code: '108',
            source: 'cheating website'
          });
        }
        return this.handleNotFound(req, res);
      }
      return resolve();
    });
  }

  runServer() {
    let self = this,
      configServer = self._config;

    return BuildRequest.buildHandleRequest
      .call(this, log)
      .then(requests => {
        return BuildRequest.buildRoutes
          .call(this, requests, log)
          .then(routes => {
            return dataStore
              .buildModel()
              .then(async () => {
                //handle permission
                this.setupPermission();

                app.use((req, res, next) => {
                  if (req.device.type === 'bot') {
                    const agent = req.device.parser.useragent;
                    if (
                      /(Android 6.0.1|Nexus 5X Build\/MMB29P)/i.test(
                        agent.source
                      )
                    ) {
                      req.device.type = 'phone';
                    }
                  }

                  req.getStore = getStore;
                  req.responseObject = BaseResponse.responseObject;
                  req.reply = BaseResponse.reply;
                  req.replyError = BaseResponse.replyError;
                  req.log = log;
                  req.next = next;
                  req.nameConfig = this._config.nameConfig;

                  if (logHelper.checkLog(req.path)) {
                    log.info({
                      headers: req.headers,
                      infoRequest: {
                        cookies: req.cookie,
                        path: req.originalUrl,
                        method: req.method,
                        httpVersion: req.httpVersion
                      },
                      body: req.body
                    });
                  }

                  req[this._config.nameConfig || 'config'] = config;

                  if (/(^\/api\/)/g.test(req.path) || this._config.onlyApi) {
                    req.reply = BaseResponse.replyApi;
                    req.replyError = BaseResponse.replyErrorApi;
                  }
                  const listRoutes = Object.keys(routes);
                  let handleRequestCurrent;
                  listRoutes.every(e => {
                    const regRouteCheckReq = new RegExp(`/${e}/`, 'g');
                    if (regRouteCheckReq.test(req.path)) {
                      if (requests[e])
                        handleRequestCurrent = Object.assign(
                          requests[e],
                          requests.common
                        );
                      return false;
                    }
                    return true;
                  });
                  if (
                    (req.config.cors && req.config.cors.open === true) ||
                    (Array.isArray(req.config.cors) &&
                      req.config.cors.includes(req.headers.origin))
                  ) {
                    if (req.headers.origin) {
                      res.setHeader(
                        'Access-Control-Allow-Origin',
                        req.headers.origin
                      );
                      res.setHeader(
                        'Access-Control-Allow-Methods',
                        'GET, POST, OPTIONS, PUT, DELETE'
                      );
                      res.setHeader(
                        'Access-Control-Allow-Headers',
                        'User-Agent,Keep-Alive,Content-Type,Authorization,Cache-Control'
                      );
                      res.setHeader('Access-Control-Allow-Credentials', true);
                    }
                  }

                  if (!handleRequestCurrent)
                    handleRequestCurrent = requests.common;

                  if (requests.request) {
                    Object.keys(requests.request).forEach(e => {
                      req[e] = requests.request[e](req, res);
                    });
                  }

                  if (requests.response) {
                    Object.keys(requests.response).forEach(e => {
                      res.locals[e] = requests.response[e](req, res);
                    });
                  }

                  return this.securePath(req, res).then(() => {
                    if (handleRequestCurrent) {
                      return BaseResponse.requestData(
                        req,
                        res,
                        handleRequestCurrent,
                        {
                          exceptKey: [
                            'userJWT',
                            'permission',
                            'userBearer',
                            'customAuth'
                          ]
                        }
                      ).then(requestData => {
                        req.commonData = requestData;
                        return next();
                      });
                    }
                    req.commonData = {};
                    return next();
                  });
                });

                device.enableViewRouting(app);

                let routeGenerate = configServer.route || Object.keys(routes);

                if (this._config.serverSocket) {
                  const docSocket = await this._opts.buildDocs();
                  app.get('/sockets.json', (req, res) => {
                    if (!docSocket) return res.json(docSocket);
                    return this._opts.buildDocs().then(json => {
                      return res.json(json);
                    });
                  });
                  const docSocketURL = '/sockets/docs';
                  this._config.sockets = this._config.sockets || {};
                  this._config.sockets.docs = this._config.sockets.docs || {};
                  app.get(docSocketURL, (req, res, next) => {
                    return res.send(
                      RenderSocket.render(
                        this._config.sockets.docs.title ||
                          'Doc socket created by nhutdev(Tran Van Nhut)'
                      )
                    );
                  });
                }

                if (this._config.swagger) {
                  const listRouteApi = Object.keys(this._genRouteSwagger);
                  listRouteApi.forEach(e => {
                    const swaggerConfig = this._config.swagger,
                      swaggerURL = `/${e}/docs`;
                    app.get(swaggerURL, (req, res, next) => {
                      return res.send(
                        Render.extract(
                          this._config.swagger.title ||
                            'Swagger edited by nhutdev(Tran Van Nhut)',
                          `/${e}`
                        )
                      );
                    });
                    app.get(`/${e}/swagger.json`, (req, res) => {
                      const swaggerJson = new GeneratorSwagger(
                        swaggerConfig,
                        this._genRouteSwagger[e],
                        {
                          jsonWebToken: this._config.jsonWebToken
                        }
                      );
                      return res.json(swaggerJson.parse);
                    });
                  });
                }

                if (this._config.postman) {
                  /**
                   * Handle postman online
                   */
                  // const parsePostman = allCollection => {
                  //   this._config.postman.forEach(e => {
                  //     const postman = new Postman(
                  //       e,
                  //       allCollection ? allCollection.collections : null
                  //     );
                  //     postman.generateJson(this._genRouteSwagger);
                  //   });
                  // };
                  // try {
                  //   const allCollection = await Merge.getAll();
                  //   parsePostman(allCollection);
                  // } catch (error) {
                  //   parsePostman();
                  // }
                  this._config.postman.forEach(e => {
                    const postman = new Postman(e, null);
                    postman.generateJson(this._genRouteSwagger);
                  });
                }

                routeGenerate.forEach(e => {
                  app.use(`/${e === 'main' ? '' : e}`, routes[e]);
                });

                const outLogError = (req, err, position) => {
                  if (req.device.type !== 'bot') {
                    if (
                      app.get('env') ===
                      (self._config.developmentEnv || 'development')
                    )
                      log.error({
                        error: typeof err === 'object' ? err.stack : err
                      });
                    else
                      log.fatal(
                        errorHelper.extractErrorLog(req, err, {
                          position: position
                        })
                      );
                  }
                };

                app.use((req, res, next) => {
                  if (
                    app.get('env') ===
                    (self._config.developmentEnv || 'development')
                  )
                    outLogError(req, 'Log route');
                  if (/(^\/api\/)/g.test(req.path) || this._config.onlyApi) {
                    if (self._config.serverSocket)
                      return req.replyError(req, res, {
                        code: '109',
                        source: 'server socket'
                      });
                    return req.replyError(req, res, {
                      code: '404',
                      source: 'route'
                    });
                  }

                  return this.handleNotFound(req, res);
                });

                if (
                  app.get('env') ===
                  (self._config.developmentEnv || 'development')
                ) {
                  app.use((err, req, res, next) => {
                    if (/(^\/api\/)/g.test(req.path) || this._config.onlyApi) {
                      return req.replyError(req, res, err);
                    }

                    return res
                      .status(err.status || 500)
                      .render(configServer.template.error, {
                        message: err.message || 'Error undefined',
                        error: err
                      });
                  });
                }

                app.use((err, req, res, next) => {
                  if (/(^\/api\/)/g.test(req.path) || this._config.onlyApi) {
                    return req.replyError(req, res, err);
                  }
                  if (err.message !== '404' && !err.code)
                    outLogError(req, err, 'Log production');
                  return this.handleNotFound(req, res);
                });

                const normalizePort = val => {
                    let port = parseInt(val, 10);
                    if (isNaN(port)) {
                      return val;
                    }
                    if (port >= 0) {
                      return port;
                    }

                    return false;
                  },
                  port = normalizePort(config.port || '3000');
                app.set('port', port);
                let createServerHttp2;
                if (self._config.http2)
                  createServerHttp2 = http2.createServer(
                    {
                      key: fs.readFileSync(self._config.http2.keyPath),
                      cert: fs.readFileSync(self._config.http2.certPath)
                    },
                    app
                  );

                const http2 = require('spdy'),
                  server = createServerHttp2 || http.Server(app),
                  onError = error => {
                    if (error.syscall !== 'listen') {
                      throw error;
                    }

                    const bind =
                      typeof port === 'string'
                        ? 'Pipe ' + port
                        : 'Port ' + port;

                    // handle specific listen errors with friendly messages
                    switch (error.code) {
                      case 'EACCES':
                        log.error(bind + ' requires elevated privileges');
                        process.exit(1);
                        break;
                      case 'EADDRINUSE':
                        log.error(bind + ' is already in use');
                        process.exit(1);
                        break;
                      default:
                        throw error;
                    }
                  },
                  onListening = () => {
                    const addr = server.address(),
                      bind =
                        typeof addr === 'string'
                          ? 'pipe ' + addr
                          : 'port ' + addr.port;
                    if (!process.env.NODE_ENV) {
                      process.env.NODE_ENV = 'development';
                    }
                    if (self._config.http2) {
                      log.info('Create server with https config:');
                      log.info({
                        key: self._config.http2.keyPath,
                        cert: self._config.http2.certPath
                      });
                    }
                    console.log(`Environment: ${process.env.NODE_ENV || ''}`);
                    console.log(
                      `Listening on ${config.mode || 'localhost'} ${bind}`
                    );
                    const linkDocs = Object.keys(this._genRouteSwagger);
                    if (Array.isArray(linkDocs) && linkDocs.length > 0) {
                      linkDocs.forEach(e => {
                        log.info(
                          `Documents link: http://${config.mode ||
                            'localhost'}:${port}/${e}/docs`
                        );
                      });
                    }
                  };

                if (this._opts && this._opts.socketHandle) {
                  this._opts.socketHandle(server, {
                    getStore: getStore,
                    config: config,
                    log: log
                  });
                }

                this.workerProcess(() => {
                  server.listen(port);
                  server.on('error', onError);
                  server.on('listening', onListening);
                });
              })
              .catch(error => {
                log.error(error);
              });
          })
          .catch(err => {
            log.error(err);
          });
      })
      .catch(err => {
        log.error(err);
      });
  }
}

module.exports = ServerExpress;

module.exports.getStore = getStore;
