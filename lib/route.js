/*
 * @Author: Tran Van Nhut (nhutdev) 
 * @Date: 2017-10-18 15:53:59 
 * @Last Modified by: Tran Van Nhut (nhutdev)
 * @Last Modified time: 2018-01-23 10:34:51
 */
const Hoek = require('hoek');
const helpers = require('helpers');

class Route {

  constructor(option) {
    this._basePath = option.basePath;
    if (option.storeName)
      this._controller = new option.controller({
        storeName: option.storeName,
        ownerForm: option.ownerForm
      });
    else
      this._controller = new option.controller();
    this._schema = option.schema;
    if (option.storeName)
      this._storeName = option.storeName.toLowerCase();
    this._keyParams = option.keyParams || 'id';
    this._includes = option.includes;
    if (option.includes)
      if (option.includes.includes('insert') || option.includes.includes('update') || option.includes.includes('get'))
        Hoek.assert(option.schema && option.schema.insert && option.schema.get, 'Route cannot empty schema');
    this._routes = [];
    this._commonConfig = option.common;
  }

  get insert() {
    return {
      url: this._basePath,
      method: 'POST',
      middleware: 'insert',
      summary: `Create ${this._storeName}`,
      description: `Return data ${this._storeName}`,
      request: {
        schema: this._schema.insert || {}
      }
    };
  }

  get update() {
    return {
      url: `${this._basePath}/:${this._keyParams}`,
      method: 'PUT',
      middleware: 'update',
      summary: `Update ${this._storeName}  by ${this._keyParams}`,
      description: `Return data ${this._storeName}`,
      request: {
        schema: this._schema.update || this.parseSchemaUpdate(helpers.Json.cloneDeep(this._schema.insert), this._schema.get)
      }
    };
  }

  parseSchemaUpdate(schema, update) {
    schema = Hoek.clone(schema);
    schema = Object.assign(schema, update || {});
    const body = schema.body;
    if (body)
      Object.keys(body).forEach(e => {
        const keyCurrent = body[e]._flags.presence;
        if (keyCurrent)
          delete body[e]._flags.presence;
      });

    return schema;
  }

  get get() {
    return {
      url: `${this._basePath}/:${this._keyParams}`,
      method: 'GET',
      middleware: 'get',
      summary: `Get one ${this._storeName} by ${this._keyParams}`,
      description: `Return data ${this._storeName}`,
      request: {
        schema: this._schema.get || {}
      }
    };
  }

  get delete() {
    return {
      url: `${this._basePath}/:${this._keyParams}`,
      method: 'DELETE',
      middleware: 'delete',
      summary: `Delete one ${this._storeName} by ${this._keyParams}`,
      description: 'Return message',
      request: {
        schema: this._schema.delete || this._schema.get
      }
    };
  }

  get list() {
    return {
      url: this._basePath,
      method: 'GET',
      middleware: 'list',
      summary: `Get list ${this._storeName}`,
      description: `Return list data ${this._storeName}`,
      request: {
        schema: this._schema.list || {},
        paging: helpers.Schemas.pagination
      }
    };
  }

  mergeCommon(routeCurrent, routeNeedMerge, key) {
    if (routeNeedMerge[key]) {
      if (key === 'request') {
        routeCurrent.request = routeCurrent.request || {};
        routeCurrent.request = Object.assign(routeCurrent.request, routeNeedMerge.request);
      } else if (key === 'middleware') {
        routeCurrent.middleware = routeNeedMerge.middleware;
      }
    }
  }

  commonGen(route) {
    if (!route.url)
      route.url = this._basePath;
    if (this._commonConfig) {
      if (this._commonConfig.request) {
        this.mergeCommon(route, this._commonConfig, 'request');
        this.mergeCommon(route, this._commonConfig, 'middleware');
      }
    }
    route.controller = this._controller;
    return route;
  }

  addRoute(config) {
    this._routes.push(this.commonGen(config));
  }

  get route() {
    if (this._includes) {
      this._includes.forEach(e => {
        if (typeof e === 'object') {
          if (this[e.name]) {
            let routeCurrent = this[e.name];
            this.mergeCommon(routeCurrent, e, 'request');
            this.mergeCommon(routeCurrent, e, 'middleware');
            this._routes.push(this.commonGen(routeCurrent));
          }
        } else {
          this._routes.push(this.commonGen(this[e]));
        }
      });
    }
    return this._routes;
  }

}

module.exports = Route;