const bunyan = require('bunyan'),
  bformat = require('bunyan-format'),
  formatOut = bformat({
    outputMode: 'short'
  });
const config = require('config');

const EmailStream = require('bunyan-emailstream').EmailStream;

class Log {
  get log() {
    config.log = config.log || {};
    let streams = [{
      level: config.log.level || 'debug',
      stream: formatOut
    }];
    if (config.log.emailLog) {
      const emailStream = new EmailStream({
        from: 'sunnyboy1019@gmail.com',
        to: config.log.emailLog.to
      }, {
        type: 'SMTP',
        service: 'gmail',
        auth: {
          user: 'sunnyboy1019@gmail.com',
          pass: 'nhutnhut'
        }
      });
      emailStream.formatBody = (log) => {
        let rows = [];

        rows.push('* name: ' + log.name);
        rows.push('* hostname: ' + log.hostname);
        rows.push('* time: ' + log.time);
        if (log.headers)
          rows.push(`* headers: ${JSON.stringify(log.headers)}`);
        if (log.infoRequest) {
          if (log.infoRequest.url)
            rows.push(`* url: ${log.infoRequest.url}`);
          if (log.infoRequest.method)
            rows.push(`* method: ${log.infoRequest.method}`);
        }
        if (log.position)
          rows.push(`* position: ${log.position}`);

        if (log.msg) {
          rows.push('* msg: ' + log.msg);
        }

        if (log.err) {
          rows.push(`* err full: ${typeof log.err === 'object' ? JSON.stringify(log.err):log.err}`);
          if (log.err.stack)
            rows.push('* err.stack: ' + log.err.stack);
        }

        if (!log.headers && !log.infoRequest && !log.position && !log.msg && !log.err)
          rows.push(`* content log: ${typeof log === 'object' ? JSON.stringify(log) : log.toString()}`);

        return rows.join('\n');
      };
      emailStream.formatSubject = (log) => {
        return `[ERROR] ${log.name}/${log.hostname} ${log.msg?log.msg:typeof log.err === 'object' ? JSON.stringify(log.err):log.err}`;
      };

      let stream = {
        type: 'raw',
        stream: emailStream,
        level: config.log.emailLog.level
      };

      streams.push(stream);
    }

    return bunyan.createLogger({
      name: config.log.name || 'Default',
      streams: streams
    });
  }

  run() {
    let log = this.log;
    log.info('Create log successfully');
  }
}

const logProgress = new Log();
logProgress.run();

module.exports = logProgress.log;