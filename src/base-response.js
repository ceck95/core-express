const BPromise = require('bluebird');
const log = require('../lib/log');
const errorHelper = require('./error');
const config = require('config');
const Hoek = require('hoek');

class BaseResponse {
  static reply(req, res, data, view, opts) {
    data = data || {};
    const dataResp = Object.assign(req.commonData ? req.commonData : {}, data),
      config = req[req.nameConfig || 'config'];
    if (!config.responseJson) {
      log.trace(`Information data${view ? ' view' : ''}: =================>`);
      log.trace({
        view: view || 'NULL',
        dataResponse: dataResp
      });
    }
    if (opts && opts.status) {
      res.status(opts.status);
    }

    if (config.responseJson) {
      return res.json(dataResp);
    }

    if (view) {
      return res.render(view, dataResp);
    }
    return res.json(dataResp);
  }

  static replyApi(req, res, data, opts) {
    let dataResp = {};
    if (opts) {
      if (opts.includeReq) data = Object.assign(data, req.commonData);
      if (opts.message) {
        if (data.meta)
          dataResp.meta = Object.assign(data.meta, {
            message: opts.message
          });
        else
          dataResp.meta = {
            message: opts.message
          };
        dataResp.data = data.meta ? data.data : data;
      }
      if (opts.resXml) {
        res.header('Content-Type', 'application/xml');
        return res.send(data);
      }
    } else {
      dataResp = data;
    }
    log.debug(dataResp);
    Hoek.assert(
      dataResp.meta,
      'Response data must have key "meta", check it now'
    );
    return res.json(dataResp);
  }

  static compactObject(data, opts) {
    return BaseResponse.responseObject(data, opts);
  }

  static responseObject(data, opts) {
    if (opts) {
      if (opts.except) {
        const filterDataExcept = obj => {
          Object.keys(obj).forEach(e => {
            if (typeof obj[e] === 'object' && obj[e] !== null)
              filterDataExcept(obj[e]);
            else if (opts.except.includes(e)) delete obj[e];
          });
          return obj;
        };
        data = filterDataExcept(data);
      }
      if (opts.includes) {
        const filterDataInclude = obj => {
          Object.keys(obj).forEach(e => {
            if (
              typeof obj[e] === 'object' &&
              !opts.keepObject &&
              obj[e] !== null &&
              !(obj[e] instanceof Date)
            ) {
              filterDataInclude(obj[e]);
            } else {
              if (!opts.includes.includes(e)) delete obj[e];
            }
          });
          return obj;
        };
        data = filterDataInclude(data);
      }
    }

    return data;
  }

  static requestData(req, res, handleRequest, opts) {
    if (handleRequest ? Object.keys(handleRequest).length > 0 : false) {
      let promiseArr = [],
        requestData = {},
        objResp = {};

      Object.keys(handleRequest).forEach(e => {
        let arrCondition = [],
          condition = true;
        if (Array.isArray(opts.exceptKey) && opts.exceptKey.length > 0) {
          opts.exceptKey.forEach(a => {
            arrCondition.push(`'${e}' !== '${a}'`);
          });

          condition = eval(arrCondition.join(' && '));
        }

        if (condition) {
          if (typeof handleRequest[e] === 'function') {
            promiseArr.push(handleRequest[e](req, res));
            requestData[e] = {};
          } else {
            objResp[e] = handleRequest[e];
          }
        }
      });
      return BPromise.all(promiseArr)
        .then(results => {
          if (Object.keys(requestData).length === results.length) {
            Object.keys(requestData).forEach((e, i) => {
              requestData[e] = results[i];
            });
            return Object.assign(objResp, requestData);
          }
        })
        .catch(err => {
          return req.replyError(req, res, err);
        });
    }
  }

  static replyError(req, res, err) {
    const errResp = err.constructor ? err : new Error(err);
    return req.next(errResp);
  }

  static exceptionError(err) {
    if (err.sql) {
      err.code = '510';
      err.source = 'exception';
    }
    return err;
  }

  static replyErrorApi(req, res, err, opts) {
    err = BaseResponse.exceptionError(err);
    opts = opts || {};
    if (config.log && config.log.emailLog) {
      if (!err.code && !Array.isArray(err))
        log.fatal(errorHelper.extractErrorLog(req, err));
    } else {
      log.error(err);
    }

    if (!Array.isArray(err)) {
      if (err.code) {
        const message = err.message || null;
        err = errorHelper.translate({
          code: err.code,
          source: err.source,
          params: err.params || null
        });
        if (Array.isArray(err) && err.length > 0) {
          err = err[0];
          if (!err.message && !err.messageUi) {
            err = errorHelper.translateWithErrorCrash({
              code: '502',
              message: message
            });
          }
        }
      } else {
        err = errorHelper.translateWithErrorCrash({
          code: '502',
          message: err.message
        });
      }
    }
    let errorCode = parseInt(opts.statusCode) || 400;
    switch (errorCode) {
      case 404:
        break;
      case 502:
        break;
      case 400:
        break;
      case 401:
        break;
      case 550:
        break;
      default:
        errorCode = 400;
        break;
    }

    if (!Array.isArray(err)) {
      err = [err];
    }

    return res.status(errorCode).json({
      errors: err
    });
  }
}

module.exports = BaseResponse;
