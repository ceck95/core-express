const helpers = require('helpers');

class RouteHelper {

  static parseStringTag(url) {
    const patternFullMatch = new RegExp('/([^\/]*?)/'),
      patternMissing = new RegExp('/(.*.?)');
    let tagsStr;
    if (patternFullMatch.test(url))
      tagsStr = url.match(patternFullMatch);
    else
      tagsStr = url.match(patternMissing);
    const src = helpers.String.toCamelCase(tagsStr[1], {
      delimiter: '-'
    });

    return RouteHelper.upperCaseFirstCharacter(src).replace(/[A-Z]/g, (x, y) => {
      return ` ${x.toUpperCase()}`;
    }).trim();
  }

  static upperCaseFirstCharacter(src) {
    return src.replace(/^\w/, (x, y) => {
      return x.toUpperCase();
    });
  }

}

module.exports = RouteHelper;