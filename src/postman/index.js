const helpers = require('helpers');
const helperRoute = require('../helper/route');
const fs = require('fs');
const Merge = require('./merge');

class Postman {
  constructor(config, allCollection) {
    this._config = config;
    this._allCollection = allCollection;
  }

  parseItem(baseUrl, element) {
    element.url = `/${baseUrl}${element.url}`;
    const parseHeader = headers => {
        let listKeyHeader = Object.keys(headers);
        listKeyHeader = listKeyHeader.map(e => {
          const currentKey = helperRoute.upperCaseFirstCharacter(e);
          let valueDefault = headers[e]._description;
          if (currentKey === 'Authorization') {
            element.request = element.request || {};
            this._config.token = this._config.token || {};
            const token = this._config.token;
            if (element.request.authorized || element.request.merge) {
              valueDefault = `Bearer ${token.bearer || null}`;
            } else {
              valueDefault = `Basic ${new Buffer(
                `${token.basic.clientSecret}.${token.basic.clientId}`
              ).toString('base64')}`;
            }
          }
          return {
            key: currentKey,
            value: valueDefault
          };
        });
        listKeyHeader.push({
          key: 'Content-Type',
          value: 'application/json'
        });
        return listKeyHeader;
      },
      parseQuery = query => {
        const listKeyQuery = Object.keys(query);
        return listKeyQuery.map(e => {
          return {
            key: e,
            value: query[e]._type,
            equals: true
          };
        });
      },
      parseBody = body => {
        return helpers.Joi.joiToObject(body);
      },
      cfgUrl = this._config.url,
      data = {
        name: element.summary,
        request: {
          method: element.method.toUpperCase(),
          url: {
            raw: `${cfgUrl.protocol}://${cfgUrl.host}${
              cfgUrl.port ? cfgUrl.port : ''
            }${element.url}`,
            protocol: cfgUrl.protocol,
            host: [cfgUrl.host],
            port: cfgUrl.port,
            path: element.url.replace(/^\//, '').split('/'),
            description: ''
          }
        },
        description: ''
      };
    const request = element.request || {};
    if (request.headers) {
      data.request.header = parseHeader(request.headers);
    }
    if (request.schema) {
      if (request.schema.query) {
        data.request.query = parseQuery(request.schema.query);
      }
      if (request.schema.body) {
        data.request.body = {
          mode: 'raw',
          raw: JSON.stringify(parseBody(request.schema.body), null, 2)
        };
      }
    }
    return data;
  }

  async generateJson(route) {
    route = helpers.Json.cloneDeep(route);
    if (helpers.Json.checkEmptyObject(route)) return true;
    const listFolder = Object.keys(route),
      content = {
        item: []
      };
    listFolder.forEach(e => {
      const folderCurrent = {
        name: e,
        description: '',
        item: []
      };
      let currentTags = [];
      route[e].forEach(a => {
        currentTags.push(helperRoute.parseStringTag(a.url));
      });
      currentTags = helpers.Array.uniqueItem(currentTags).map(b => {
        return {
          name: b,
          description: '',
          item: []
        };
      });
      route[e].forEach(c => {
        currentTags.forEach(d => {
          if (d.name === helperRoute.parseStringTag(c.url)) {
            d.item.push(this.parseItem(e, c));
          }
        });
      });

      folderCurrent.item = currentTags;
      content.item.push(folderCurrent);
    });
    const dataWriter = {
      info: {
        name: this._config.name,
        description: this._config.description || '',
        schema:
          'https://schema.getpostman.com/json/collection/v2.1.0/collection.json'
      },
      ...content
    };
    let currentCollection;
    if (this._allCollection)
      this._allCollection.every(e => {
        if (e.name == this._config.name) {
          currentCollection = e;
          return false;
        }
        return true;
      });

    if (currentCollection) {
      console.log(
        `Collection '${this._config.name}' with uid ${currentCollection.uid}`
      );
      const collection = await Merge.getOne(currentCollection.uid),
        items = collection.collection.item,
        merge = (item, api, route) => {
          const itemCompare = items
            .find(e => e.name === api)
            .item.find(e => e.name === route)
            .item.find(e => e.name === item.name);
          if (itemCompare) {
            if (item.request.body) {
              item.request.body.raw = itemCompare.request.body.raw;
            }
            item.request.method = itemCompare.request.method;
            item.request.url = itemCompare.request.url;
          }
          return item;
        };
      dataWriter.item = dataWriter.item.map(apis => {
        apis.item = apis.item.map(routes => {
          routes.item = routes.item.map(e => {
            return merge(e, apis.name, routes.name);
          });
          return routes;
        });
        return apis;
      });
    } else {
      console.log(`Don't exist collection '${this._config.name}' online`);
    }

    const basePath = this._config.basePath || process.cwd(),
      path = `${basePath}/postman`;
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path);
    }

    return fs.writeFile(
      `${path}/${this._config.name}.json`,
      JSON.stringify(dataWriter, null, 2),
      'utf8',
      err => {
        if (err) {
          console.error(err);
          return false;
        }
        console.log(`Build json for postman successfully at path: ${path}`);
        console.log(`Build successfully with name ${this._config.name}`);
        return true;
      }
    );
  }
}

module.exports = Postman;
