const Hoek = require('hoek');
const config = require('config');

const env = process.env.NODE_ENV || 'development';
const defaultConfig = require(`${process.cwd()}/config/default`);

try {
  const envConfig = require(`${process.cwd()}/config/${env}`);
  Hoek.merge(defaultConfig, envConfig, false, false);
} catch (e) {
  Hoek.assert(false, e.toString());
}

process.env.NODE_CONFIG = JSON.stringify(defaultConfig);

module.exports = config.util.loadFileConfigs();