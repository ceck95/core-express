const BPromise = require('bluebird');
const fs = BPromise.promisifyAll(require('fs'));
const Path = require('path');
const adapterDir = 'adapter';
const Hoek = require('hoek');
const log = require('../lib/log');
const helpers = require('helpers');

class DataStore {
  constructor() {
    this.dataStore = {};
  }

  buildModel() {
    return new BPromise((resolve, reject) => {
      const adapterPath = Path.join(process.cwd(), adapterDir),
        dataStore = {};
      log.info('Building model and adapter for project ...');
      const driverNameMySQL = 'MySQL',
        driverNameMongo = 'MongoDB',
        driveNamePostgresSQL = 'PostgresSQL';
      return fs.readdirAsync(adapterPath).then(async files => {
        if (!files || files.length === 0) {
          log.info('Build model and adapter successfully');
          return resolve(this.dataStore);
        }

        for (const i in files) {
          const e = files[i],
            pathCurrent = Path.join(process.cwd(), adapterDir, e),
            adapter = require(pathCurrent),
            instanceName = adapter.name;
          if (instanceName.indexOf('Adapter') < 0)
            Hoek.assert(false, `${instanceName} name is not correct format`);

          const nameClassModel = helpers.String.toSnakeCase(
              instanceName.replace('Adapter', ''),
              {
                delimiter: '-'
              }
            ),
            classAdapter = await adapter.instance(
              instanceName,
              nameClassModel,
              adapter
            ),
            driver = classAdapter.logName;
          let nameModel = 'default';

          switch (driver) {
            case driverNameMySQL:
              nameModel = classAdapter.model.constructor.name;
              break;
            case driverNameMongo:
            case driveNamePostgresSQL:
              nameModel = classAdapter.constructor.name.replace('Adapter', '');
              break;
          }
          dataStore[nameModel] = classAdapter;
          if (files.length - 1 == i) {
            log.info('Build model and adapter successfully');
            this.dataStore = dataStore;
            return resolve(this.dataStore);
          }
        }
      });
    }).catch(e => {
      throw e;
    });
  }

  getStore(name) {
    Hoek.assert(
      this.dataStore[name],
      `DataStore ${name} not exist , please check adapter model or model`
    );
    return this.dataStore[name];
  }
}

module.exports = DataStore;
