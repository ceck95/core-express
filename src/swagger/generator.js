/*
 * @Author: Tran Van Nhut (nhutdev) 
 * @Date: 2017-10-20 20:23:35 
 * @Last Modified by: Tran Van Nhut (nhutdev)
 * @Last Modified time: 2018-04-24 14:51:57
 */
const helpers = require('helpers');
const DESC_400 = 'Bad request';
const DESC_403 = 'Forbidden';
const DESC_404 = 'Data not found';
const DESC_401 = 'Unauthorized';
const DESC_500 =
  'The server encountered an unexpected condition which prevented it from fulfilling the request';
const routeHelpers = require('../helper/route');

class GeneratorSwagger {
  constructor(config, routes, configServer) {
    this._swagger = config;
    this._routes = routes;
    this._swagger.swagger = '2.0';
    this._configServer = configServer;
    this._swagger.paths = this.buildRoutes(this._routes);
    this.initModel();
  }

  parseDefinitionsType(arr) {
    return Array.isArray(arr) ? 'array' : typeof arr;
  }

  initModel() {
    const schemas = helpers.Schemas;
    if (!this._swagger.definitions) this._swagger.definitions = {};
    this._swagger.definitions.error = {
      items: {
        $ref: `#/definitions/${this.parseDefinitions(schemas.error[0])}`,
        type: typeof schemas.error[0]
      },
      type: this.parseDefinitionsType(schemas.error)
    };
  }

  buildRoutes(routes) {
    let routesGen = [];
    routes.forEach(e => {
      let item = {
        url: e.url
      };
      item[e.method] = e;
      routes.forEach(a => {
        if (e.url === a.url && e.method !== a.method) {
          item[a.method] = a;
        }
      });
      if (routesGen.length > 0) {
        let checkPush = true;
        routesGen.every(e => {
          if (e.url === item.url) {
            checkPush = false;
            return false;
          }
          return true;
        });
        if (checkPush) {
          routesGen.push(item);
        }
      } else {
        routesGen.push(item);
      }
    });

    let parseSwaggerRoute = {};
    routesGen.forEach(e => {
      const tagsStr = routeHelpers.parseStringTag(e.url);
      const keyRouteCurrent = Object.keys(e);
      keyRouteCurrent.forEach(k => {
        if (k !== 'url') {
          const parameters = this.buildParameter(e[k].request),
            schemaError = {
              $ref: '#/definitions/error',
              type: 'object'
            };
          const parseUrlRequest = url => {
            if (!/\/$/g.test(url)) {
              url = `${url}/`;
            }
            if (/:/g.test(url)) {
              url = url.replace(/:(.*?)\//g, (x, y) => {
                return x.replace(':', '{').replace('/', '}/');
              });
            }
            url = url.replace(/\/$/g, (x, y) => {
              return x.replace('/', '');
            });
            return url;
          };
          if (!parseSwaggerRoute[parseUrlRequest(e.url)])
            parseSwaggerRoute[parseUrlRequest(e.url)] = {};
          parseSwaggerRoute[parseUrlRequest(e.url)][k] = {
            produces: ['application/json'],
            tags: [tagsStr],
            description: e[k].description || 'Description default',
            parameters: parameters,
            summary: e[k].summary || 'Summary default',
            responses: {
              '400': {
                description: DESC_400,
                schema: schemaError
              },
              '401': {
                description: DESC_401,
                schema: schemaError
              },
              '403': {
                description: DESC_403,
                schema: schemaError
              },
              '404': {
                description: DESC_404,
                schema: schemaError
              },
              '500': {
                description: DESC_500,
                schema: schemaError
              }
            }
          };
        }
      });
    });

    return parseSwaggerRoute;
  }

  parseType(type, opts) {
    let str;
    switch (type) {
      case 'any':
        str = 'string';
        break;
      case 'number':
        if (opts) {
          if (opts.inStr === 'path' || opts.inStr == 'query') {
            if (opts.required) {
              str = 'integer';
            } else {
              str = 'double';
            }
          }
        } else {
          str = type;
        }

        break;
      default:
        str = type;
        break;
    }
    return str;
  }

  buildParameter(rule, swagger) {
    this._swagger.definitions = this._swagger.definitions || {};
    if (rule) {
      let obj, query;
      if (rule.paging) {
        query = Object.assign(
          rule.schema ? rule.schema.query : {} || {},
          rule.paging.query
        );
        if (!rule.schema) rule.schema = {};
        rule.schema.query = query;
      }
      obj = rule.schema || {};

      const parseJoiToSwagger = (keyObj, name, inStr, opts) => {
        opts = opts || {};
        let required = false;
        if (Object.keys(keyObj._flags).length > 0) {
          if (keyObj._flags.presence === 'required') {
            required = true;
          }
        }
        let item = {
          in: inStr,
          name: name,
          type: this.parseType(keyObj._type, {
            required: required,
            inStr: inStr
          })
        };
        if (opts.desc) item.description = opts.desc;
        if (required === true) {
          item.required = required;
        }
        return item;
      };
      let parameters = [];
      if (obj.params) {
        Object.keys(obj.params).forEach(e => {
          parameters.push(parseJoiToSwagger(obj.params[e], e, 'path'));
        });
      }

      if (rule.headers) {
        Object.keys(rule.headers).forEach(e => {
          parameters.push(
            parseJoiToSwagger(rule.headers[e], e, 'header', {
              desc: rule.headers[e]._description
            })
          );
        });
      }

      if (obj.query) {
        Object.keys(obj.query).forEach(e => {
          parameters.push(parseJoiToSwagger(obj.query[e], e, 'query'));
        });
      }
      if (obj.body) {
        const definitionName = this.parseDefinitions(obj.body);
        parameters.push({
          in: 'body',
          name: 'body',
          schema: {
            type: this.parseDefinitionsType(obj.body),
            $ref: `#/definitions/${definitionName}`
          }
        });
      }
      if (obj.files) {
        Object.keys(obj.files).forEach(e => {
          parameters.push({
            in: 'formData',
            name: e,
            type: 'file',
            description: 'The file to upload.'
          });
        });
      }
      return parameters;
    }
    return [];
  }

  checkProperty(properties, data, type) {
    let check = true;
    if (type === 'alternatives') {
      const matches = data._inner.matches[0],
        example = matches
          ? `Value input have type: '${
              matches.then ? matches.then._type : undefined
            }' or '${matches.otherwise ? matches.otherwise._type : undefined}'`
          : 'Type alternatives';
      properties.type = 'string';
      properties.example = example;
    } else if (type === 'date') {
      properties.type = 'string';
      properties.example = 'format date';
    } else {
      check = false;
    }
    return check;
  }

  parseDefinitions(data) {
    let definitions = {
      properties: {}
    };
    const joiArrayToObject = src => {
      let result = {};
      if (!Array.isArray(src)) return result;
      src.forEach(e => {
        result[e.key] = e.schema;
      });
      return result;
    };

    Object.keys(data).forEach(e => {
      if (data[e].isJoi) {
        if (!definitions.properties[e]) definitions.properties[e] = {};
        if (data[e]._type === 'array' && data[e]._inner.items[0]) {
          const obj = joiArrayToObject(data[e]._inner.items[0]._inner.children);
          if (helpers.Json.checkEmptyObject(obj)) {
            definitions.properties[e] = {
              items: {
                type: this.parseType(data[e]._inner.items[0]._type)
              }
            };
          } else {
            definitions.properties[e] = {
              items: {
                $ref: `#/definitions/${this.parseDefinitions(obj)}`,
                type: typeof obj
              },
              type: data[e]._type
            };
          }
        } else if (data[e]._type === 'object') {
          const obj = joiArrayToObject(data[e]._inner.children);
          definitions.properties[e] = {
            $ref: `#/definitions/${this.parseDefinitions(obj)}`
          };
        } else {
          const currentType = this.parseType(data[e]._type);
          if (
            !this.checkProperty(definitions.properties[e], data[e], currentType)
          ) {
            definitions.properties[e].type = currentType;
            if (data[e]._valids._set.length > 1) {
              definitions.properties[e].example = data[e]._valids._set.join(
                '|'
              );
            }
          }
          definitions.type = typeof data;
        }
      } else {
        definitions.properties[e] = {
          $ref: `#/definitions/${this.parseDefinitions(data[e])}`
        };
      }
    });

    if (!this._swagger.definitions) this._swagger.definitions = {};
    if (definitions && Object.keys(this._swagger.definitions).length > 0) {
      let existDefinitions = null;
      Object.keys(this._swagger.definitions).every(e => {
        if (
          JSON.stringify(this._swagger.definitions[e]) ===
          JSON.stringify(definitions)
        ) {
          existDefinitions = e;
          return false;
        }
        return true;
      });
      if (existDefinitions) return existDefinitions;
    }
    const definitionName = `Model ${
      Object.keys(this._swagger.definitions).length
    }`;
    if (!this._swagger.definitions[definitionName])
      this._swagger.definitions[definitionName] = definitions;
    return definitionName;
  }

  // camelCaseFirstCharacter(src) {
  //   src = helpers.String.toCamelCase(src, {
  //     delimiter: '-'
  //   });

  //   return src.replace(/^\w/, (x, y) => {
  //     return x.toUpperCase();
  //   }).replace(/[A-Z]/g, (x, y) => {
  //     return ` ${x.toUpperCase()}`;
  //   }).trim();
  // }

  get parse() {
    return this._swagger;
  }
}

module.exports = GeneratorSwagger;
