const Joi = require('joi');
const Hoek = require('hoek');

class Error {

  static validate(schema, data) {
    const optionJoi = {
      abortEarly: false
    };
    return Joi.validate(data, schema, optionJoi, (errs, value) => {
      let errors = [];
      if (errs) {
        errs.details.forEach(e => {
          let code = Error.getCode(e.type);
          errors.push({
            code: code,
            message: e.message,
            source: e.path,
            messageUi: Error.translateMessage({
              code: code,
              params: {
                field: e.path
              }
            }).messageUi
          });
        });

        return errors;

      }
      return errors;
    });
  }

  static loadFileResource() {
    let resourcesErrorVN = require('../src/resources/error/vn.json'),
      resourcesErrorGlobal = require('../src/resources/error/en.json');
    const pathResources = `${process.cwd()}/resources/error/`,
      pathResourcesVN = `${pathResources}vn.json`,
      pathResourcesEN = `${pathResources}en.json`;
    let resourcesVN = {},
      resourcesEN = {};
    try {
      resourcesVN = require(pathResourcesVN);
      resourcesEN = require(pathResourcesEN);
    } catch (err) {
      //no handle
    }

    resourcesErrorVN = Object.assign(resourcesErrorVN, resourcesVN);
    resourcesErrorGlobal = Object.assign(resourcesErrorGlobal, resourcesEN);
    return {
      resourcesErrorVN: resourcesErrorVN,
      resourcesErrorGlobal: resourcesErrorGlobal
    };
  }

  static translateMessage(err) {
    const strErrorVNDefault = 'Không tìm thấy message',
      strErrorGlobalDefault = 'Not found message';
    let strErrorVN, strErrorGlobal;

    if (typeof err === 'object') {
      const resources = Error.loadFileResource();
      if (err.code) {
        strErrorVN = resources.resourcesErrorVN[err.code.toString()];
        strErrorGlobal = resources.resourcesErrorGlobal[err.code.toString()];
      }
      if (err.params) {

        Object.keys(err.params).forEach(e => {
          strErrorVN = strErrorVN.replace(`{{${e}}}`, err.params[e]);
          strErrorGlobal = strErrorGlobal.replace(`{{${e}}}`, err.params[e]);
        });

      }
    } else {
      if (err) {
        strErrorVN = err;
      }
    }
    return {
      message: strErrorGlobal || strErrorVNDefault,
      messageUi: strErrorVN || strErrorGlobalDefault
    };

  }

  static translate(error) {
    const err = Error.translateMessage(error);
    return [{
      code: error.code,
      message: err.message,
      source: error.source || '',
      messageUi: err.messageUi
    }];
  }

  static translateWithErrorCrash(error) {
    const err = Error.translateMessage(error);
    return [{
      code: error.code,
      message: error.message,
      source: 'core',
      messageUi: err.messageUi
    }];
  }

  static getCode(type) {
    let code = '502';
    const listCode = {
      'any.empty': '100',
      'any.required': '100',
      'string.min': '101',
      'number.min': '103',
      'number.max': '104',
      'string.max': '102',
      'string.email': '103',
      'string.base': '110',
      'number.base': '111'
    };
    Object.keys(listCode).every(e => {
      if (e === type) {
        code = listCode[e];
        return false;
      }
      return true;
    });
    return code;
  }

  static extractErrorLog(req, err, opts) {
    opts = opts || {};
    return {
      headers: req.headers,
      infoRequest: {
        url: `${req.protocol}://${req.hostname}${req.originalUrl}`,
        method: req.method,
      },
      position: opts.position,
      err: typeof err === 'object' ? err.stack ? err.stack : err : err
    };
  }

}
module.exports = Error;